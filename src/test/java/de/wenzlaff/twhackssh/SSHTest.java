package de.wenzlaff.twhackssh;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
class SSHTest {

	@Test
	void isConnecte() throws Exception {

		String username = "kleinhirn";
		String password = "unsicher";
		String host = "pi-zero";
		int port = 22;
		int timeout = 1000;

		assertTrue(SSH.isConnecte(username, password, host, port, timeout));
	}

	@Test
	void isNotConnecte() {

		String username = "kleinhirn";
		String password = "falsches-passwort";
		String host = "pi-zero";
		int port = 22;
		int timeout = 1000;

		assertFalse(SSH.isConnecte(username, password, host, port, timeout));
	}

	@Test
	void isKeinServer() {

		String username = "kleinhirn";
		String password = "falsches-passwort";
		String host = "kein-server";
		int port = 22;
		int timeout = 1000;

		Assertions.assertThrows(IllegalArgumentException.class, () -> SSH
				.isConnecte(username, password, host, port, timeout));
	}
}