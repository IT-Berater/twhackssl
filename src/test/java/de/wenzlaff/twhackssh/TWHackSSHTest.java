package de.wenzlaff.twhackssh;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import picocli.CommandLine;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
class TWHackSSHTest {

	@Test
	public void testHilfeMitFehlendenParameter() throws Exception {

		TWHackSSH app = new TWHackSSH();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("--Help");
		assertEquals(2, exitCode);
	}

	@Test
	public void testHilfeKurz() throws Exception {

		TWHackSSH app = new TWHackSSH();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("-h");
		assertEquals(0, exitCode);
	}

	@Test
	public void testPositivParameter() throws Exception {

		TWHackSSH app = new TWHackSSH();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("-n", "22", "-s", "pi-zero", "-t", "1000");
		assertEquals(0, exitCode);
	}

	@Test
	public void testVersionsParameter() throws Exception {

		TWHackSSH app = new TWHackSSH();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("-V");
		assertEquals(0, exitCode);
	}
}