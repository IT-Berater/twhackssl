package de.wenzlaff.twhackssh;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
class DateiTest {

	@Test
	public void leseUserDatei() throws Exception {

		String wert = "kleinhirn";
		Path path = Paths.get("src/test/resources/users.txt");

		String read = Files.readAllLines(path).get(2);
		assertEquals(wert, read);
	}

	@Test
	public void lesePasswortDatei() throws Exception {

		String wert = "unsicher";
		Path path = Paths.get("src/test/resources/passwords.txt");

		String read = Files.readAllLines(path).get(2);
		assertEquals(wert, read);
	}
}