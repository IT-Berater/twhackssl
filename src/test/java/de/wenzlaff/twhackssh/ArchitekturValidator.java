package de.wenzlaff.twhackssh;

import org.apache.logging.log4j.Logger;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.CompositeArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;

/**
 * Architektur vorgaben.
 * 
 * Siehe https://www.archunit.org/userguide/html/000_Index.html
 * 
 * @author Thomas Wenzlaff
 */
@AnalyzeClasses(packages = "de.wenzlaff")
public class ArchitekturValidator {

	/**
	 * Klasse hat kein Zugriff auf System.in , System.out or System.err
	 * 
	 * Klasse soll kein java util logging nutzen
	 * 
	 * Klasse soll kein joda time
	 * 
	 * Methoden sollen keine Generic Exception werfen
	 */
	@ArchTest
	static final ArchRule Allgemeine_Regel = CompositeArchRule
			.of(GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS)
			.and(GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS)
			.and(GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING)
			.and(GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME)
			.because("Allgemeine Architektur Regel");

	/**
	 * Apache Log4J Logger nur private, final und static und Feldname LOG
	 */
	@ArchTest
	static final ArchRule Logger_Regel = fields().that()
			.haveRawType(Logger.class).should().bePrivate().andShould()
			.beStatic().andShould().beFinal().andShould().haveName("LOG")
			.because("Eigene Architektur Regel");
}