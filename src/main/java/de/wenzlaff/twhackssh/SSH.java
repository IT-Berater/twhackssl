package de.wenzlaff.twhackssh;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * SSH Verbindungs Klasse.
 * 
 * @author Thomas Wenzlaff
 */
public class SSH {

	private static final Logger LOG = LogManager.getLogger(SSH.class);

	/**
	 * Checkt ob SSH Verbindung möglich ist.
	 * 
	 * @param username der Username
	 * @param password das Passwort
	 * @param host     der Host oder Server
	 * @param port     der SSH Port
	 * @param timeout  die Timeout Zeit in Millisekunden
	 * @return boolean true, wenn Verbindung zum Server geklappt hat, false wenn
	 *         nicht
	 */
	public static boolean isConnecte(String username, String password,
			String host, int port, int timeout) {

		Session session = null;
		boolean ergebnis = true;
		try {
			try {
				session = new JSch().getSession(username, host, port);
				session.setPassword(password);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setTimeout(timeout);
				session.connect();
			} catch (JSchException e) {
				if (e.getLocalizedMessage().contains("UnknownHostException")) {
					LOG.error("Host: " + host + " nicht erreichbar");
					throw new IllegalArgumentException(host);
				}
				ergebnis = false;
			}
			LOG.info("Verbindung: " + ergebnis + " User: " + username
					+ " Passwort: " + password);
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
		return ergebnis;
	}
}