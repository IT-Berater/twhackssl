package de.wenzlaff.twhackssh;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Datei {

	private static final Logger LOG = LogManager.getLogger(Datei.class);

	public static List<String> getDaten(String dateiname) {
		Path path = Paths.get(dateiname);
		List<String> daten;
		try {
			daten = Files.readAllLines(path);
		} catch (IOException e) {
			LOG.error("Konnte die Datei " + path + " nicht finden. ");
			return new ArrayList<>();
		}
		return daten;
	}
}