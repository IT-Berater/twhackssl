package de.wenzlaff.twhackssh;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * CMD für SSH Scanner.
 * 
 * @author Thomas Wenzlaff
 */
@Command(name = "TWHackSSH", mixinStandardHelpOptions = true, version = "TWHackSSH 1.0.8", description = "TWHackSSH", showDefaultValues = true, footer = {
		"@|fg(green) Thomas Wenzlaff|@", "@|fg(red),bold http://www.wenzlaff.info|@" })
public class TWHackSSH implements Callable<Integer> {

	private static final Logger LOG = LogManager.getLogger(TWHackSSH.class);

	@Option(names = { "-s", "--server" }, description = "SSH Server", required = true)
	private static String host = "pi-zero";

	@Option(names = { "-n", "--portnummer" }, description = "SSH Port Nummer", defaultValue = "22")
	private static int port = 22;

	@Option(names = { "-t", "--timeout" }, description = "Timeout in Millisekunden", defaultValue = "1000")
	private static int timeout = 1000;

	@Option(names = { "-u", "--usernamen" }, description = "Dateiname für die Username Liste", defaultValue = "users.txt")
	private static String dateinameUser;

	@Option(names = { "-p", "--passwörter" }, description = "Dateiname für die Passwort Liste", defaultValue = "passwords.txt")
	private static String dateinamenPassword = "unsicher";

	public static void main(String[] args) throws Exception {
		LOG.info("Start TWHackSSH ...");

		CommandLine cl = new CommandLine(new TWHackSSH());
		int exitCode = cl.execute(args);
		logLogoKleinhirn();
		System.exit(exitCode);
	}

	private static void logLogoKleinhirn() {
		LOG.info(" ▄    ▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄               ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄ ");
		LOG.info("▐░▌  ▐░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌             ▐░░░░░░░░░░░▌▐░▌       ▐░▌");
		LOG.info("▐░▌▐░▌  ▐░▌          ▐░▌               ▐░▌     ▐░▌▐░▌    ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌▐░▌    ▐░▌             ▐░▌          ▐░▌       ▐░▌");
		LOG.info("▐░▌░▌   ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄      ▐░▌     ▐░▌ ▐░▌   ▐░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░▌ ▐░▌   ▐░▌             ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌");
		LOG.info("▐░░▌    ▐░▌          ▐░░░░░░░░░░░▌     ▐░▌     ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌             ▐░░░░░░░░░░░▌▐░▌       ▐░▌");
		LOG.info("▐░▌░▌   ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀      ▐░▌     ▐░▌   ▐░▌ ▐░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░█▀▀▀▀█░█▀▀ ▐░▌   ▐░▌ ▐░▌             ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌");
		LOG.info("▐░▌▐░▌  ▐░▌          ▐░▌               ▐░▌     ▐░▌    ▐░▌▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌     ▐░▌  ▐░▌    ▐░▌▐░▌             ▐░▌          ▐░▌       ▐░▌");
		LOG.info("▐░▌ ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄█░█▄▄▄▄ ▐░▌     ▐░▐░▌▐░▌       ▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌      ▐░▌ ▐░▌     ▐░▐░▌      ▄      ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌");
		LOG.info("▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌      ▐░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌      ▐░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌");
		LOG.info(" ▀    ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀        ▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ ");
	}

	@Override
	public Integer call() throws Exception {

		List<String> users = Datei.getDaten(dateinameUser);
		List<String> passwoerter = Datei.getDaten(dateinamenPassword);

		LOG.info("Verwende SSH Port: " + port + " Timeout: " + timeout + " ms zu Host: " + host);

		boolean status = false;
		for (String user : users) {
			for (String passwort : passwoerter) {
				status = SSH.isConnecte(user, passwort, host, port, timeout);
				if (status) {
					LOG.error("Verbunden deshalb ABBRUCH mit User: " + user + " Passwort: " + passwort + " Host: " + host);
					return 1;
				}
			}
		}
		return 0;
	}
}